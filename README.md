Library ini menggunakan CURL, sehingga dibutuhkan web server yang telah terinstall CURL. Umumnya PHP versi terbaru telah terinstall CURL secara default. Untuk menginstall CURL dapat dilihat di http://php.net/manual/en/curl.installation.php.

Setup :
1. Buka KartukuDirectAPI.php, isi MERCHANT_TOKEN dan SECRET_KEY dengan MERCHANT_TOKEN dan SECRET_KEY anda.
2. Isi production dengan false untuk koneksi ke sendbox dan true untuk koneksi ke produksi

File : 
index.php merupakan halaman root yang berisi link contoh proses transaksi
direct_authorize.php merupakan contoh form transaksi authorize
direct_capture.php merupakan contoh form transaksi authorize
direct_purchase.php merupakan contoh form transaksi purchase
direct_query.php merupakan contoh form transaksi query
direct_refund.php merupakan contoh form transaksi refund
direct_token_list.php merupakan contoh form transaksi token list
direct_token_remove.php merupakan contoh form transaksi token remove
direct_token_store.php merupakan contoh form transaksi token store
direct_void_capture.php merupakan contoh form transaksi void capture
direct_void_purchase.php merupakan contoh form transaksi void purchase
direct_void_refund.php merupakan contoh form transaksi void refund
KartukuDirectAPI.php merupakan library php yang digunakan untuk proses transaksi ke direct API Kartuku IPG
process_authorize.php merupakan contoh implementasi kode transaksi authorize
process_capture.php merupakan contoh implementasi kode transaksi capture
process_purchase.php merupakan contoh implementasi kode transaksi purchase
process_query.php merupakan contoh implementasi kode transaksi query
process_refund.php merupakan contoh implementasi kode transaksi refund
process_token_list.php merupakan contoh implementasi kode transaksi token list
process_token_remove.php merupakan contoh implementasi kode transaksi token list
process_token_store.php merupakan contoh implementasi kode transaksi token store
process_voidcapture.php merupakan contoh implementasi kode transaksi void capture
merupakan contoh implementasi kode transaksi void purchase
process_voidrefund.php merupakan contoh implementasi kode transaksi void refund